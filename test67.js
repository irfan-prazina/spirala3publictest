
let GodineAjax = require('./GodineAjax.js');
let ZadaciAjax = require('./ZadaciAjax.js');
const assert = require('assert');
const sinon = require('sinon');
const MockBrowser = require('mock-browser').mocks.MockBrowser;
const chaiHttp = require("chai-http");
const chai = require("chai");

const mock = new MockBrowser();
global.document = mock.getDocument();
let div = document.createElement('div');



chai.use(chaiHttp);


describe("Zadatak 6",function(){
    let requests;
    beforeEach(function() {
        global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();
        requests=this.requests=[];
        XMLHttpRequest.onCreate = function(xhr){
            requests.push(xhr);
        }
    });
    afterEach(function () {
        XMLHttpRequest.restore();
    });
    it('salje AJAX zahtjev', function (done) {
        let ga = new GodineAjax(div);
        assert.equal(1, requests.length);
        assert.equal(requests[0].url.indexOf("localhost:8080/godine") >= 0, true, "Ne šalje zahtjev na dobar url! treba biti http://localhost:8080/godine, a kod vas je " + requests[0].url);
        done();
    })
    it('salje AJAX zahtjev kod osvjezi metode',function(done){
        let ga = new GodineAjax(div);
        requests[0].respond(200,{"Content-Type":"application/json"},'[]');
        assert.equal(1, requests.length);
        ga.osvjezi();
        assert.equal(2,requests.length);
        done();
    })
})


describe("Zadatak 8",function(){
    let requests;
    beforeEach(function() {
        global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();
        requests=this.requests=[];
        XMLHttpRequest.onCreate = function(xhr){
            requests.push(xhr);
        }
    });
    afterEach(function () {
        XMLHttpRequest.restore();
    });
    it('salje AJAX zahtjev', function (done) {
        let za = new ZadaciAjax(function(x){});
        za.dajXML();
        assert.equal(1, requests.length);
        assert.equal(requests[0].url.indexOf("localhost:8080/zadaci") >= 0, true, "Ne šalje zahtjev na dobar url! treba biti http://localhost:8080/zadaci, a kod vas je " + requests[0].url);
        assert.equal(requests[0].requestHeaders["Accept"].indexOf("application/xml")>=0,true,"Niste postavili Accept header");
        done();
    })
})