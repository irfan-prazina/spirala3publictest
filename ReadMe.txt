###Kopirajte direktorij spirala3publictest u direktorij vašeg projekta!###
Ukucajte u komandnoj liniji:
	npm install

Da testirate zadatke 1,2,3,4,5,7:
- pokrenite vaš server pa u ovom direktoriju u komandnoj liniji ukucajte
	npx mocha test.js

Da testirate zadatke 6 i 8
- kopirajte datoteke GodineAjax.js i ZadaciAjax.js u ovaj direktorij
- u datoteku GodineAjax.js na kraj dodajte module.exports=GodineAjax;
- u datoteku ZadaciAjax.js na kraj dodajte module.exports=ZadaciAjax;
- u komandnoj liniji ukucajte:
	npx mocha test67.js


Napomena: Ovo nisu finalni testovi, ako vam svi testovi prolaze ne znači da ćete imati sve bodove!
